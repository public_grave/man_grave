`libinput-list-devices` - список локальных устройств, распознаваемых libinput, и значений по умолчанию их конфигурации.

# СИНТАКСИС

	sudo libinput list-devices [--help]
