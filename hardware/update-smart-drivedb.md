`update-smart-drivedb` - обновление базы данных дисков smartmontools в файле `/usr/share/smartmontools/drivedb.h` или DESTFILE.

# СИНТАКСИС

	update-smart-drivedb [ПАРАМЕТРЫ] [DESTFILE]

# ПРИМЕРЫ

	update-smart-drivedb
