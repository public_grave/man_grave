Команда `findmnt` выводит список всех смонтированных файловых систем или выполняет поиск файловой системы. Команда `findmnt` может искать в /etc/fstab, /etc/mtab или /proc/self/mountinfo. Если устройство или точка монтирования не указаны, будут показаны все файловые системы. Параметр командной строки `--target` принимает любой файл или каталог, после чего `findmnt` отображает файловую систему для заданного пути. По умолчанию команда выводит все смонтированные файловые системы в древовидном формате. Команда `findmnt` отображает целевую точку монтирования (TARGET), исходное устройство (SOURCE), тип файловой системы (FSTYPE) и соответствующие параметры монтирования, например, является ли файловая система доступной только для чтения/записи или только для чтения (OPTIONS).

# СИНТАКСИС

	findmnt [параметры]
	findmnt [параметры] устройство|точка монтирования
	findmnt [параметры] [--source] устройство [--target path|--mountpoint точка монтирования]

# ПАРАМЕТРЫ

- `-t, --types list` - Ограничить выбор печатаемых файловых систем. В списке, разделенном запятыми, может быть указано более одного типа.
- `-x, --verify` - Проверка содержимого таблицы монтирования. По умолчанию проверяется /etc/fstab. 
