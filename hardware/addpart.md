Команда `addpart` используется для информирования ядра Linux о существовании указанного раздела. Эта команда представляет собой простую обёртку для "add partition" `ioctl`. Она не взаимодействует с разделами на жестком диске.

# СИНТАКСИС

	addpart device partition start length

# ВЕРСИЯ

	util-linux 2.39.4
	2024-01-31
