`badblocks` используется для поиска поврежденных блоков на устройстве (обычно разделе диска).

Важное замечание: если вывод `badblocks` будет передаваться программам `e2fsck` или `mke2fs`, важно правильно указать размер блока, поскольку генерируемые номера блоков очень сильно зависят от размера блока, используемого файловой системой. По этой причине настоятельно рекомендуется не запускать `badblocks` напрямую, а использовать опцию `-c` в программах `e2fsck` и `mke2fs`.

# СИНТАКСИС

	badblocks  [ -svwnfBX ] [ -b block_size ] [ -c blocks_at_once ] [ -d read_delay_factor ] [ -e max_bad_blocks ] [ -i input_file ] [
    -o output_file ] [ -p num_passes ] [ -t test_pattern ] device [ last_block ] [ first_block ]
