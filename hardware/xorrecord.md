`xorrecord` — это эмулятор программы CD/DVD/BD cdrecord, который записывает предварительно отформатированные данные на CD, DVD и BD носители.

# СИНТАКСИС

	xorrecord [options] dev=device [track_source]
