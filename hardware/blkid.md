`blkid` - поиск/вывод атрибутов блочного устройства. `lsblk` предоставляет больше информации, лучше контролирует форматирование вывода, легко используется в сценариях и не требует прав root для получения актуальной информации. `blkid` считывает информацию непосредственно с устройств и для пользователей, не являющихся root, возвращает кэшированную непроверенную информацию.

# СИНТАКСИС

	blkid --label label | --uuid uuid
	
	blkid [--no-encoding --garbage-collect --list-one --cache-file file] [--output format] [--match-tag tag] [--match-token
    NAME=value] [device...]
	
	blkid --probe [--offset offset] [--output format] [--size size] [--match-tag tag] [--match-types list] [--usages list]
    [--no-part-details] device...
	
	blkid --info [--output format] [--match-tag tag] device...
