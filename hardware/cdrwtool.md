Команда `cdwrtool` может выполнять определенные действия на устройстве CD-R, CD-RW или DVD-R. В основном это очистка носителя, его форматирование для использования с устройством packet-cd и создание файловой системы UDF. Наиболее часто используется опция `быстрая настройка`: `cdrwtool -d device -q`, которая очистит диск, отформатирует его как одну большую дорожку и запишет структуры файловой системы UDF.

# СИНТАКСИС

	drwtool -d device -i | -g

    cdrwtool -d device -s [ write-parameters ]

    cdrwtool -d device -q [ write-parameters ]

    cdrwtool -d device -m offset [ write-parameters ]

    cdwrtool -d device -u blocks [ write-parameters ]

    cdrwtool -d device -b b_mode [ write-parameters ]

    cdrwtool -d device -c blocks [ write-parameters ]

    cdwrtool -d device -f filename [ write-parameters ]

    cdwrtool -d device -r track [ write-parameters ]

	cdrwtool -h
