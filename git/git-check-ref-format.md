`git-check-ref-format` - Проверка правильности формирования имени ссылки.

Проверяет, является ли заданное refname допустимым, и завершает работу с ненулевым статусом, если это не так. Ссылка используется в Git'е для указания ветвей и тегов. Головка ветки хранится в иерархии refs/heads, а тег - в иерархии refs/tags пространства имён ref (обычно в каталогах `$GIT_DIR/refs/heads` и `$GIT_DIR/refs/tags` или в виде записей в файле `$GIT_DIR/packed-refs`, если refs упаковываются `git gc`).

# СИНТАКСИС

	git check-ref-format [--normalize] [--[no-]allow-onelevel] [--refspec-pattern] <refname>
    git check-ref-format --branch <branchname-shorthand>
