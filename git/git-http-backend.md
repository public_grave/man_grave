`git-http-backend` - реализация Git'а через HTTP на стороне сервера.

Простая CGI-программа для обслуживания содержимого Git-репозитория Git-клиентами, получающими доступ к репозиторию по протоколам http:// и https://. По умолчанию включен только сервис upload-pack, который обслуживает клиентов git fetch-pack и git ls-remote, вызываемых из `git fetch`, `git pull` и `git clone`. Если клиент аутентифицирован, включается служба receive-pack, которая обслуживает клиентов git send-pack, который вызывается из `git push`.

# СИНТАКСИС

	git http-backend
