Разделяет почтовые сообщения в почтовом ящике на сообщения журнала коммитов, информацию об авторстве и патчи, и применяет их к текущей ветке. Можно представить это как обратную операцию `git-format-patch`, выполняемую на ветке с прямой историей без слияний.

# СИНТАКСИС

	git am [--signoff] [--keep] [--[no-]keep-cr] [--[no-]utf8] [--no-verify]
           [--[no-]3way] [--interactive] [--committer-date-is-author-date]
           [--ignore-date] [--ignore-space-change | --ignore-whitespace]
           [--whitespace=<action>] [-C<n>] [-p<n>] [--directory=<dir>]
           [--exclude=<path>] [--include=<path>] [--reject] [-q | --quiet]
           [--[no-]scissors] [-S[<keyid>]] [--patch-format=<format>]
           [--quoted-cr=<action>]
           [--empty=(stop|drop|keep)]
           [(<mbox> | <Maildir>)...]
    git am (--continue | --skip | --abort | --quit | --show-current-patch[=(diff|raw)] | --allow-empty)
