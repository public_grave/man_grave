`git-mailsplit` - простая программа разбиения mbox под UNIX.

Разбивает файл mbox или Maildir на список файлов: «0001» «0002» ... в указанную директорию, чтобы вы могли обрабатывать их дальше оттуда. Разбиение Maildir зависит от сортировки имен файлов для вывода патчей в правильном порядке.

# СИНТАКСИС

	git mailsplit [-b] [-f<nn>] [-d<prec>] [--keep-cr] [--mboxrd]
                       -o<directory> [--] [(<mbox>|<Maildir>)...]
