`Arp` управляет или отображает кэш соседей сети IPv4 ядра. Он может добавлять записи в таблицу, удалять их или отображать текущее содержимое.

# СИНТАКСИС

	arp [−vn] [−H type] [−i if] [−ae] [hostname]
	arp [−v] [−i if] −d hostname [pub]
	arp [−v] [−H type] [−i if] −s hostname hw_addr [temp]
	arp [−v] [−H type] [−i if] −s hostname hw_addr [netmask nm] pub
	arp [−v] [−H type] [−i if] −Ds hostname ifname [netmask nm] pub
	arp [−vnD] [−H type] [−i if] −f [filename]
