`domainname` - показать или установить системное доменное имя NIS/YP.

# СИНТАКСИС

   domainname [nisdomain] [-F file]
