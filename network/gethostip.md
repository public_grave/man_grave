Утилита `gethostip` преобразует заданное имя хоста или IP-адрес в различные форматы.

# СИНТАКСИС

	gethostip [-dxnf]  [HOSTNAME|IP]
