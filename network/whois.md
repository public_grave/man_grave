Команда `whois` используется для получения информации о владельце доменного имени или IP-адреса.

# СИНТАКСИС

	whois [ПАРАМЕТРЫ] [ЗАПРОС]

# ПРИМЕРЫ

	whois google.com
	whois 8.8.8.8
