`ip-link` - конфигурация сетевых устройств.

# СИНТАКСИС

	ip link  { COMMAND | help }

       ip link add [ link DEVICE ] [ name ] NAME
               [ txqueuelen PACKETS ]
               [ address LLADDR ] [ broadcast LLADDR ]
               [ mtu MTU ] [ index IDX ]
               [ numtxqueues QUEUE_COUNT ] [ numrxqueues QUEUE_COUNT ]
               [ gso_max_size BYTES ] [ gso_ipv4_max_size BYTES ] [ gso_max_segs SEGMENTS ]
               [ gro_max_size BYTES ] [ gro_ipv4_max_size BYTES ]
               [ netns { PID | NETNSNAME | NETNSFILE } ]
               type TYPE [ ARGS ]

       ip link delete { DEVICE | group GROUP } type TYPE [ ARGS ]

       ip link set { DEVICE | group GROUP }
               [ { up | down } ]
               [ type ETYPE TYPE_ARGS ]
               [ arp { on | off } ]
               [ dynamic { on | off } ]
               [ multicast { on | off } ]
               [ allmulticast { on | off } ]
               [ promisc { on | off } ]
               [ protodown { on | off } ]
               [ protodown_reason PREASON { on | off } ]
               [ trailers { on | off } ]
               [ txqueuelen PACKETS ]
               [ gso_max_size BYTES ] [ gso_ipv4_max_size BYTES ] [ gso_max_segs SEGMENTS ]
               [ gro_max_size BYTES ] [ gro_ipv4_max_size BYTES ]
               [ name NEWNAME ]
               [ address LLADDR ]
               [ broadcast LLADDR ]
               [ mtu MTU ]
               [ netns { PID | NETNSNAME | NETNSFILE } ]
               [ link-netnsid ID ]
               [ alias NAME ]
               [ vf NUM [ mac LLADDR ]
                        [ VFVLAN-LIST ]
                        [ rate TXRATE ]
                        [ max_tx_rate TXRATE ]
                        [ min_tx_rate TXRATE ]
                        [ spoofchk { on | off } ]
                        [ query_rss { on | off } ]
                        [ state { auto | enable | disable } ]
                        [ trust { on | off } ]
                        [ node_guid eui64 ]
                        [ port_guid eui64 ] ]
               [ { xdp | xdpgeneric | xdpdrv | xdpoffload } { off |
                       object FILE [ { section | program } NAME ] [ verbose ] |
                       pinned FILE } ]
               [ master DEVICE ]
               [ nomaster ]
               [ vrf NAME ]
               [ addrgenmode { eui64 | none | stable_secret | random } ]
               [ macaddr [ MACADDR ]
                         [ { flush | add | del } MACADDR ]
                         [ set MACADDR ] ]

                 ip link show [ DEVICE | group GROUP ] [ up ] [ master DEVICE ] [ type ETYPE ] [ vrf NAME ] [ nomaster ]

                 ip link xstats type TYPE [ ARGS ]

                 ip link afstats [ dev DEVICE ]

                 ip link help [ TYPE ]

                 TYPE := [ amt | bareudp | bond | bridge | can | dsa | dummy | erspan | geneve | gre | gretap | gtp | hsr | ifb |
                         ip6erspan | ip6gre | ip6gretap | ip6tnl | ipip | ipoib | ipvlan | ipvtap | lowpan | macsec | macvlan | macvtap |
                         netdevsim | nlmon | rmnet | sit | vcan | veth | virt_wifi | vlan | vrf | vti | vxcan | vxlan | xfrm ]

                 ETYPE := [ TYPE | bridge_slave | bond_slave ]

                 VFVLAN-LIST := [ VFVLAN-LIST ] VFVLAN

                 VFVLAN := [ vlan VLANID [ qos VLAN-QOS ] [ proto VLAN-PROTO ] ]

         ip link property add dev DEVICE [ altname NAME .. ]

         ip link property del dev DEVICE [ altname NAME .. ]
