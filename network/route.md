`route` манипулирует таблицами IP-маршрутизации ядра. В основном она используется для установки статических маршрутов к определенным узлам или сетям через интерфейс после его конфигурирования с помощью программы `ifconfig`. При использовании параметров `add` или `del` `route` изменяет таблицы маршрутизации. Без этих параметров `route` отображает текущее содержимое таблиц маршрутизации.

# СИНТАКСИС

	route [−CFvnNee] [−A family |−4|−6]
	route [−v] [−A family |−4|−6] add [−net|−host] target [netmask Nm] [gw Gw] [metric N] [mss M] [window W] [irtt I] [reject] [mod] [dyn] [reinstate] [[dev] If]
	route [−v] [−A family |−4|−6] del [−net|−host] target [gw Gw] [netmask Nm] [metric M] [[dev] If]
	route [−V] [−−version] [−h] [−−help]
