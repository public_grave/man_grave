`dnsmasq` — отличный сервер для служб имен в локальных сетях, который можно использовать как для системы доменных имен (Domain Name System, DNS), так и для динамического обнаружения хостов (Dynamic Host Discovery Protocol, DHCP). Dnsmasq также поддерживает протоколы BOOTP, PXE и TFTP, предназначенные для загрузки по сети и установки операционных систем с сетевого сервера. Dnsmasq поддерживает IPv4 и IPv6, обеспечивает локальное кэширование имен и действует как тупиковый резолвер (stub resolver).

# СИНТАКСИС

	dnsmasq [ПАРАМЕТРЫ]...

Использование файла /etc/hosts — очень старый способ настройки DNS, основанный на описании соответствий имен хостов и IP-адресов в статическом файле. Файла /etc/hosts вполне достаточно для очень маленьких сетей.

`dnsmasq` разрабатывался для организации служб имен в локальных сетях. Это легковесный и простой в настройке сервер, особенно по сравнению с BIND, доминирующим DNS-сервером, довольно тяжеловесным и требующим определенного обучения обращению с ним.

`dnsmasq` и /etc/hosts отлично работают вместе. `dnsmasq` читает записи из /etc/hosts в DNS.

Сервер DHCP в `dnsmasq` автоматически интегрируется с DNS. Чтобы `dnsmasq` создавал DNS-записи для ваших DHCP-клиентов, вы должны лишь настроить DHCP-клиентов, чтобы они отправляли свои имена хостов на DHCP-сервер, что и так сделано по умолчанию в большинстве дистрибутивов Linux.

Существует четыре типа DNS-серверов: рекурсивные резолверы, корневые серверы имен, серверы имен домена верхнего уровня (Top-Level Domain, TLD) и полномочные серверы имен.

Рекурсивные резолверы отвечают на запросы DNS. Тупиковый резолвер, такой как `dnsmasq` и `systemd-resolved`, пересылает любые запросы, ответ на которые он не может получить из своего кэша, вышестоящему резолверу. Когда вы пытаетесь открыть страницу сайта, рекурсивный резолвер отыскивает DNS-информацию о сайте, посылая запросы DNS-серверам трех других типов. Рекурсивные резолверы кэшируют эту информацию, чтобы ускорить доступ к ней в будущем. Серверы имен вашего интернет-провайдера и такие службы, как OpenDNS, Cloudflare и Google Public DNS, являются рекурсивными резолверами.

Существует 13 типов корневых серверов имен, разбросанных по всей планете. Всего в настоящее время имеется несколько сотен корневых серверов имен. Корневой сервер принимает запрос от рекурсивного резолвера, пересылает его соответствующему серверу имен домена верхнего уровня (TLD): .com, .net, .org, .me, .biz, .int, .biz, .gov, .edu и т. д. Все эти серверы и домены курирует корпорация по управлению доменными именами и IP-адресами (Internet Corporation for Assigned Names and Numbers, ICANN).

Полномочные (authoritative) серверы имен служат источниками записей для домена и контролируются владельцем домена. `dnsmasq` тоже может служить вашим полномочным сервером имен, хотя я рекомендую использовать BIND.

У вас должна иметься возможность использовать `dnsmasq` в качестве сервера DNS для `NetworkManager`, поскольку `NetworkManager` имеет для этого специализированный плагин.

Вам не нужно запускать `systemd-resolved` на своем сервере `dnsmasq`, поскольку он будет конфликтовать с `dnsmasq`, состязаясь за контроль над системным тупиковым DNS-резолвером.

# Установка

Установите пакет `dnsmasq`. В этом рецепте сервер `dnsmasq` называется dns-server. Для настройки сервера DNS мы будем использовать как `dnsmasq`, так и файл /etc/hosts.

После установки остановите службу `dnsmasq`, если она была запущена:

	systemctl status dnsmasq.service
	sudo systemctl stop dnsmasq.service

Назначьте своему серверу `dnsmasq` статический IP-адрес, если он его еще не имеет. Это можно сделать на графической панели управления `NetworkManager` (`nm-connection-editor`) или в командной строке, запустив команду `nmcli`.

Следующий пример демонстрирует, как с помощью `nmcli` отыскать свои активные подключения к сети:

	nmcli connection show --active
	NAME UUID TYPE DEVICE
	1local 3e348c97-4c5f-4bbf-967e wifi wlan1
	1wired 0460d735-e14d-3c3f-92c0 ethernet eth1

Затем назначьте статический IP-адрес своему серверу DNS, указав в команде имя подключения (из столбца NAME):

	nmcli con mod "1wired" \
	ipv4.addresses "192.168.1.30/24" \
	ipv4.gateway "192.168.1.1" \
	ipv4.method "manual"

Теперь перезапустите `NetworkManager`:

	sudo systemctl restart NetworkManager.service

Далее проверьте — запущена ли служба `systemd-resolved.service`:

	systemctl status systemd-resolved.service

openSUSE Leap 15.2 не использует службу `systemd-resolved.service`, поэтому в данном дистрибутиве не нужно вносить какие-либо изменения в настройки systemd, чтобы разрешить `dnsmasq` управлять разрешением имен в вашей локальной сети. В Fedora 33 и выше, а также в Ubuntu 17.04 и выше служба `systemd-resolved.service` запускается автоматически и ее нужно отключить на сервере `dnsmasq`.

# Тестирование сервера Dnsmasq с машины клиента

Воспользуйтесь командой `dig` на любом хосте в сети и отправьте запрос по IP-адресу вашего сервера `dnsmasq`, чтобы получить информацию о любом сайте:

	dig @192.168.1.10 oreilly.com
	; <<>> DiG 9.16.6 <<>> @192.168.1.10 oreilly.com
	; (1 server found)
	;; global options: +cmd
	;; Got answer:
	;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 29387
	;; flags: qr rd ra; QUERY: 1, ANSWER: 2, AUTHORITY: 0, ADDITIONAL: 1

	;; OPT PSEUDOSECTION:
	; EDNS: version: 0, flags:; udp: 4096
	;; QUESTION SECTION:
	;oreilly.com. IN A

	;; ANSWER SECTION:
	oreilly.com. 240 IN A 199.27.145.65
	oreilly.com. 240 IN A 199.27.145.64
	
	;; Query time: 108 msec
	;; SERVER: 192.168.1.10#53(192.168.1.10)
	;; WHEN: Mon May 24 17:49:32 PDT 2021
	;; MSG SIZE rcvd: 72


Успешность теста подтверждает строка status: NOERROR и IP-адрес сервера Dnsmasq в строке SERVER.

Работу сервера имен также можно протестировать, использовав имя хоста и полное доменное имя вашего сервера вместо IP-адреса.

	dig @dns-server oreilly.com
	dig @dns-server.sqr3l.nut oreilly.com

# Настройка `dnsmasq` на роль сервера DNS для локальной сети

Любые хосты, перечисленные в файле /etc/hosts, должны иметь статические IP-адреса, и `dnsmasq` автоматически добавит их в таблицу DNS. Для начала добавьте свой сервер `dnsmasq`. В следующем примере содержимое /etc/hosts включает сервер `dnsmasq`, сервер резервного копирования и внутренний веб-сервер:

	127.0.0.1 localhost
	::1 localhost ip6-localhost ip6-loopback
	192.168.43.81 dns-server
	192.168.43.82 backups
	192.168.43.83 https

Теперь можно приступать к настройке `dnsmasq`. Переименуйте конфигурационный файл с настройками по умолчанию, чтобы начать с нового пустого файла, а прежний использовать для справки:

	sudo mv /etc/dnsmasq.conf /etc/dnsmasq.conf-old
	sudo nano /etc/dnsmasq.conf

Скопируйте следующие настройки конфигурации, подставив во второй параметр listen-address IP-адрес своего сервера, и укажите свое доменное имя. Вышестоящие серверы имен в этом примере — серверы OpenDNS, но вы можете использовать любые другие вышестоящие серверы имен, какие пожелаете. По умолчанию `dnsmasq` ищет имена в файле /etc/resolv.conf, но вообще лучше зафиксировать такой порядок поиска явно:

    # глобальные параметры
	resolv-file=/etc/resolv.conf
	domain-needed  
	bogus-priv  
	expand-hosts  
	domain=sqr3l.nut  
	local=/sqr3l.nut/  
	listen-address=127.0.0.1
	listen-address=192.168.43.81

    # вышестоящие серверы имен
	server=208.67.222.222
	server=208.67.220.220

Запустите синтаксическую проверку файла:

	dnsmasq --test
	dnsmasq: syntax check OK.

Эта проверка не обнаруживает ошибки в конфигурации — только опечатки. Запустите `dnsmasq`, и если в настройках есть какие-то ошибки, то он не запустится.

В следующем примере показан успешный запуск:

	sudo systemctl start dnsmasq.service
	systemctl status dnsmasq.service
	● dnsmasq.service - dnsmasq - A lightweight DHCP and caching DNS server
	Loaded: loaded (/lib/systemd/system/dnsmasq.service; enabled; vendor preset:
	enabled)
	Active: active (running) since Mon 2021-05-24 17:13:48 PDT; 1min 0s ago
	Process: 11023 ExecStartPre=/usr/sbin/dnsmasq --test (code=exited,
	status=0/SUCCESS)
	Process: 11024 ExecStart=/etc/init.d/dnsmasq systemd-exec (code=exited,
	status=0/SUCCESS)
	Process: 11033 ExecStartPost=/etc/init.d/dnsmasq systemd-start-resolvconf
	(code=exited, status=0/SUCCESS)
	Main PID: 11032 (dnsmasq)
	Tasks: 1 (limit: 18759)
	Memory: 2.5M
	CGroup: /system.slice/dnsmasq.service
	└─11032 /usr/sbin/dnsmasq -x /run/dnsmasq/dnsmasq.pid -u dnsmasq -7
	/etc/dnsmasq.d,.dpkg-dist,.dpkg-old,.dpkg-new --local->

	May 24 17:13:48 dns-server systemd[1]: Starting dnsmasq - A lightweight DHCP and
	caching DNS server...
	May 24 17:13:48 dns-server dnsmasq[11023]: dnsmasq: syntax check OK.
	May 24 17:13:48 dns-server systemd[1]: Started dnsmasq - A lightweight DHCP and
	caching DNS server.

Проверьте работу сервера `dnsmasq`, попробовав определить IP-адрес хоста сервера по его имени и FQDN с помощью команды `nslookup`:

	nslookup dns-server
	Server: 127.0.0.1
	Address: 127.0.0.1#53

	Name: dns-server
	Address: 192.168.43.81

	nslookup dns-server.sqr3l.nut
	Server: 127.0.0.1
	Address: 127.0.0.1#53
	
	Name: dns-server.sqr3l.nut
	Address: 192.168.43.81

	nslookup 192.168.43.81
	18.43.168.192.in-addr.arpa name = host1.sqr3l.nut.

Используйте команду `ss` для проверки открытых портов. В следующем примере столбцы Recv+Q, Send+Q и Peer Address:Port были опущены для ясности:

	sudo ss -lp "sport = :domain"
	Netid   State    Local Address:Port   Process
	udp     UNCONN   127.0.0.1:domain     users:(("dnsmasq",pid=1531,fd=8))
	udp     UNCONN   192.168.1.10:domain  users:(("dnsmasq",pid=1531,fd=6))
	tcp     LISTEN   127.0.0.1:domain     users:(("dnsmasq",pid=1531,fd=9))
	tcp     LISTEN   192.168.1.10:domain  users:(("dnsmasq",pid=1531,fd=7))

Вы должны увидеть адрес своего сервера, адрес локального хоста и только dnsmasq в столбце Process. Добавьте параметр `-r`, чтобы увидеть имена хостов вместо IP-адресов.

Если все эти команды выполнились успешно, значит, ваша конфигурация верна.

Если `dnsmasq` не запускается, то запустите `journalctl -ru dnsmasq`, чтобы узнать причину. (Если журналы `dnsmasq` отправляются куда-то еще, посмотрите там).

Команда `nslookup` входит в состав пакета bindutils.

`ss` (socket statistics — «статистика сокетов») входит в состав пакета iproute2.

Если команды `nslookup` потерпели фиаско, то попробуйте перезапустить сеть, а затем перезапустить `dnsmasq`. Если вас вновь постигла неудача, то перезагрузитесь. Если и это не помогло, то тщательно проверьте все настройки.

domain-needed не позволяет `dnsmasq` пересылать запросы на поиск простых имен хостов вышестоящим серверам имен. Если имя не указано в файле /etc/hosts или в DHCP, то возвращается ответ «не найдено». Это предотвращает утечку запросов адресов из локальной сети во внешний мир и, возможно, получение неправильного ответа, если вдруг имя домена вашей локальной сети совпадет с именем общедоступного домена.

bogus-priv блокирует обратный поиск поддельных частных адресов. В ответ на все попытки обратного поиска в диапазонах частных IP-адресов, отсутствующих в файле /etc/hosts или в файле аренды DHCP, возвращается ответ «нет такого домена» вместо передачи вышестоящим серверам имен.

expand-hosts автоматически добавляет имя частного домена к простым именам хостов в файле /etc/hosts.

domain = — имя локального домена.

`local = / [домен] /` требует от `dnsmasq` разрешать запросы для локального домена напрямую, а не пересылать их вышестоящим серверам.

# Настройка клиентов DHCP для автоматического создания записей в DNS

Единственное, что требуется от клиентов, — отправить свои имена хостов DHCP-серверу `dnsmasq`, который используется по умолчанию в большинстве Linux.

Предположим, что клиент DHCP в локальном домене sqr3l.nut имеет имя хоста client4. Когда client4 запускается, он получает свой IP-адрес и другую сетевую информацию от `dnsmasq`. `dnsmasq`, в свою очередь, получает имя хоста client4 и вводит его в DNS. После этого другие хосты в сети смогут обращаться к клиенту по именам client4 и client4.sqr3l.nut.

В файле /etc/hosts не должно быть повторяющихся записей для client4.

Есть три разных способа проверить конфигурацию клиента DHCP: в файле dhclient.conf, на графической панели настройки `NetworkManager` (`nm-connection-editor`) и с помощью команды `nmcli`.

Сначала проверьте настройки службы `dhclient`, которая в течение многих лет использовалась в Linux по умолчанию. В большинстве дистрибутивов Linux конфигурационный файл находится в /etc/dhcp/dhclient.conf. Найдите следующую строку, которая автоматически определяет имя хоста и отправляет его на сервер DHCP: `send host-name = gethostname();`. Или строку, явно определяющую имя хоста: `send host-name = myhostname`.

Если в системе нет файла dhclient.conf, то вашим клиентом DHCP управляет `NetworkManager`. Убедиться в этом можно, запустив графическую панель `nm-connection-editor`.

Если выбран метод подключения Automatic (DHCP) (Автоматически), значит, `NetworkManager` отправляет имя хоста на сервер DHCP. Если выбран метод Automatic (addresses only) (Автоматически (только адреса)), значит, имя хоста не отправляется серверу DHCP, а только загружаются настройки DNS.

Кроме того, вы можете использовать команду `nmcli`. Сначала найдите активное сетевое соединение:

	nmcli connection show --active
	NAME UUID TYPE DEVICE
	wifi1 3e348c97-4c5f-4bbf-967e-7624f3e1e4f0 wifi wlan1

Затем проверьте, посылает ли клиент имя хоста серверу DHCP. Следующий пример подтверждает, что посылает:

	nmcli connection show wifi1 | grep send-hostname
	ipv4.dhcp-send-hostname: yes
	ipv6.dhcp-send-hostname: yes

Если вы увидите no (нет), то выполните следующие команды, чтобы включить отправку. После этого перезагрузите конфигурацию:

	sudo nmcli con mod wifi1 ipv4.dhcp-send-hostname yes
	sudo nmcli con mod wifi1 ipv6.dhcp-send-hostname yes
	sudo nmcli con reload

# Управление службой DHCP с помощью `dnsmasq`

Добавьте следующие строки в файл /etc/dnsmasq.conf, чтобы определить единый пул адресов, подставив свои адреса:

    # Диапазон DHCP
	dhcp-range=192.168.1.25,192.168.1.75,12h
	dhcp-lease-max=25

Перезапустите `dnsmasq`:

	sudo systemctl restart dnsmasq.service

Попробуйте получить адрес на компьютере в локальной сети, но сначала убедитесь, что он настроен на получение IP-адреса через DHCP:

	nmcli con show --active
	NAME UUID TYPE DEVICE
	1net de7c00e7-8e4d-45e6-acaf ethernet eth0

	nmcli con show 1net | grep ipv..method
	ipv4.method: auto
	ipv6.method: auto

Вывод значения auto подтверждает, что данный компьютер использует DHCP. (Если вместо auto вы увидите manual, то это значит, что компьютер использует статические настройки.) Остановите сетевой интерфейс и снова запустите его:

	sudo nmcli con down 1net
	Connection '1net' successfully deactivated (D-Bus active path: /org/freedesktop/
	NetworkManager/ActiveConnection/11

	sudo nmcli con up 1net
	Connection successfully activated (D-Bus active path: /org/freedesktop/NetworkMan
	ager/ActiveConnection/15)

Проверьте содержимое журнала сервера `dnsmasq`:

	journalctl -ru dnsmasq
	-- Logs begin at Sun 2021-02-28 14:35:01 PST, end at Mon 2021-05-31 17:36:04
	PDT. --
	May 31 17:34:56 dns-server dnsmasq-dhcp[8080]: DHCPACK(eth0) 192.168.1.45
	9c:ef:d5:fe:01:7c client2
	May 31 17:34:56 dns-server dnsmasq-dhcp[8080]: DHCPREQUEST(eth0) 192.168.1.45
	9c:ef:d5:fe:01:7c

В данном случае записи в журнале сообщают, что назначение IP-адреса сервером dns-server клиенту client2 прошло успешно.

Вместо `nmcli` можно использовать апплет панели `NetworkManager` или запустить команду `nm-connection-editor`, чтобы открыть графический диалог настройки `NetworkManager`, в котором можно отключиться и подключиться одним щелчком кнопкой мыши.

Большинство дистрибутивов Linux задействуют `NetworkManager` для управления клиентом DHCP. Если у вас это не так, то, вероятнее всего, используется команда `dhclient`. Найдите конфигурационный файл dhclient.conf, если он существует, а затем запросите новую аренду IP-адреса с помощью команды `dhclient`:

	sudo dhclient -v
	Internet Systems Consortium DHCP Client 4.3.6-P1
	Copyright 2004-2018 Internet Systems Consortium.
	All rights reserved.
	For info, please visit https://www.isc.org/software/dhcp/

	Listening on LPF/eth0/9c:ef:d5:fe:01:7c
	Sending on LPF/eth0/9c:ef:d5:fe:01:7c
	Sending on Socket/fallback
	DHCPREQUEST on eth0 to 255.255.255.255 port 67 (xid=0xec8923)
	DHCPACK from 192.168.1.10 (xid=0xec8923)
	bound to 192.168.1.27 -- renewal in 1415 seconds.

Через DHCP можно отправлять клиентам дополнительную информацию, необходимую для доступа к сетевым службам.

dhcp-range=192.168.1.25,192.168.10.75,24h определяет диапазон из 50 адресов, доступных для аренды на срок до 24 часов. Этот диапазон не должен включать IP-адреса вашего сервера `dnsmasq` и любых других хостов со статическими IP-адресами. Продолжительность аренды можно задавать в секундах, минутах или часах. По умолчанию адрес арендуется на один час, минимальная продолжительность аренды — две минуты. Если вам нужна бессрочная аренда, то не указывайте ее продолжительность.

dhcp-lease-max=25 определяет, сколько адресов может быть арендовано одновременно. Вы можете иметь большой пул адресов и ограничивать количество одновременно действующих аренд.

# Назначение статических IP-адресов с помощью DHCP

Используйте параметр dhcp-host в файле /etc/dnsmasq.conf. Определите имя хоста клиентского компьютера и назначьте требуемый IP-адрес из блока адресов вашей локальной сети. (Для статических адресов не обязательно применять адреса из диапазона адресов DHCP, определенного с помощью параметра `_dhcp-range=*` в /etc/dnsmasq.conf.) В следующем примере хосту server2 назначается адрес, принадлежащий сети 192.168.3.0/24: `dhcp-host=server2,192.168.3.45`.

Перезапустите `dnsmasq`, после этого, когда server2 запросит адрес, он получит адрес, указанный в параметре dhcp-host=.

Добавьте несколько параметров dhcp-host=, чтобы настроить назначение статических адресов нескольким клиентам.

Вместо имени хоста можно также использовать MAC-адреса клиентов.

# Настройка подстановочных доменов

Создать подстановочный домен в `dnsmasq`, чтобы запросы к поддоменам в этом домене разрешались без ручного добавления поддоменов в DNS.

Используйте параметр address в файле /etc/dnsmasq.conf, чтобы создать домен верхнего уровня (Top-Level Domain, TLD): `address=/wildcard.net/192.168.1.35`

Перезапустите `dnsmasq`, затем выполните проверку с помощью команды `nslookup`:

	sudo systemctl restart dnsmasq.service
	nslookup foo.wildcard.net
	Server: 127.0.0.1
	Address: 127.0.0.1#53

	Name: foo.wildcard.net
	Address: 192.168.1.35

Имя foo.wildcard.net было разрешено, значит, все работает.

Используйте возможность подстановки доменов в DNS с осторожностью. Они могут пригодиться при разработке сложных служб, таких как Kubernetes. При этом всегда используйте диапазоны адресов, не пересекающиеся с диапазоном адресов на сервере имен вашей локальной сети и доступные только для клиентов локальной сети.

# Создание зон DHCP для подсетей

Имеется две подсети, и требуется настроить `dnsmasq` для распространения в них отличающихся настроек, таких как адреса маршрутизаторов и серверов по умолчанию.

Определите зоны с любыми именами по своему выбору, например zone1 и zone2, и задайте диапазоны их адресов:

	dhcp-range=zone1,192.168.50.20,192.168.50.120
	dhcp-range=zone2,192.168.60.20,192.168.60.50,24h

Зоны имеют разные маршрутизаторы:

	dhcp-option=zone1,3,192.168.50.1
	dhcp-option=zone2,3,192.168.60.2

Используют один сервер DNS:

	dhcp-option=zone1,6,192.168.1.10
	dhcp-option=zone2,6,192.168.1.10

Зона zone2 имеет сервер NTP:

	dhcp-option=zone2,42,192.168.60.15

Лишь часть параметров DHCP имеет практическую пользу.

# Передача важной информации о службах через DHCP

Организовать передачу клиентам настроек доступа к различным серверам в локальной сети через DHCP.

Некоторые настройки служб, такие как маршрут по умолчанию к интернет-шлюзу, DNS-серверу и NTP-серверу, могут автоматически передаваться клиентам в локальной сети. В следующих примерах показано, что добавить в файл /etc/dnsmasq.conf для автоматической передачи настроек некоторых служб.

Настройка маршрутизатора по умолчанию: `dhcp-option=3,192.168.1.1`

Настройка сервера DNS: `dhcp-option=6,192.168.1.10`

Ниже представлен пример, показывающий, как передать клиентам путь к локальному серверу NTP: `dhcp-option=42,192.168.1.11`

Как узнать, какие номера параметров использовать? Следующая команда выведет их все:

	dnsmasq --help dhcp
	Known DHCP options:
	1 netmask
	2 time-offset
	3 router
	6 dns-server
	7 log-server
	9 lpr-server
	[...]

Команда `dnsmasq --help dhcp` покажет известные номера параметров конфигурации DHCPv4.

# Управление журналированием в Managing

`dnsmasq` может отправлять свои сообщения в файл по вашему выбору, используя устаревший демон `syslog` вместо `journalctl`, и вы хотите узнать, какой вариант лучше.

Выбор метода совершенно неважен: и в том и в другом случае сохраняется одна и та же информация. По умолчанию журналирование выполняется в журнал systemd.

Иногда удобно изолировать журналы `dnsmasq` в отдельном каталоге, например /var/log/dnsmasq/dnsmasq.log. Используйте параметр log-feature= в файле /etc/dnsmasq.conf, чтобы указать файл журнала для использования, а затем перезапустите `dnsmasq`. Файл должен уже существовать, иначе `dnsmasq` не запустится.

Если не настроить ротацию журналов, то ваш файл журнала может стать очень большим. Следующий пример показывает, как настроить простую еженедельную ротацию в /etc/logrotate.d/dnsmasq:

	/var/log/dnsmasq/dnsmasq.log {
	missingok
	compress
	notifempty
	rotate 4
	weekly
	create
	}

Проверьте настройки с помощью команды logrotate:

	sudo /etc/logrotate.conf --debug

Отсутствие сообщений об ошибках говорит, что все в порядке.

systemd поддерживает оба демона, `journalctl` и `syslog`. Вероятно, они будут существовать вместе еще очень долго, поэтому вы можете настроить журналирование любым удобным для себя способом.

# Устранение конфликтов между `systemd-resolved` с `NetworkManager` и `dnsmasq`

Проверьте, запущена ли служба systemd-resolved.service:

	systemctl status systemd-resolved.service

В данном случае служба запущена. Она отлично подходит на роль тупикового резолвера DNS для клиентских машин, но не для DNS-серверов. Отключите ее:

	sudo systemctl stop systemd-resolved.service
	sudo systemctl disable systemd-resolved.service

Затем отыщите файл /etc/resolv.conf, который должен быть символической ссылкой:

	ls -l /etc/resolv.conf
	lrwxrwxrwx 1 root root 39 May 21 20:38 /etc/resolv.conf ->
	../run/systemd/resolve/stub-resolv.conf

Эта символическая ссылка управляется службой `systemd-resolved.service`. Чтобы отключить управление из данной службы, удалите символическую ссылку и создайте текстовый файл с тем же именем:

	sudo rm /etc/resolv.conf
	sudo touch /etc/resolv.conf

Теперь, когда /etc/resolv.conf — это обычный файл, а не символическая ссылка, им будет управлять `NetworkManager`. Откройте конфигурационный файл `NetworkManager` и найдите раздел `[main]`, затем добавьте или измените значение параметра dns= на none:

	sudo nano /etc/NetworkManager/NetworkManager.conf

	[main]
	dns=none

Добавьте в файл /etc/resolv.conf адреса локального хоста IPv4 и IPv6 сервера `dnsmasq` и свой локальный домен, если он у вас есть:

	search sqr3l.nut
	nameserver 127.0.0.1
	nameserver ::1

Затем перезагрузите и настройте `dnsmasq`.

`NetworkManager` и служба `systemd-resolved` хорошо подходят для клиентских машин. Но на сервере DNS контроль над файлом /etc/resolv.conf должен иметь только `dnsmasq` и быть единственным тупиковым резолвером.

# Настройка поддержки DNS и DHCP в `firewalld`

Откройте порты TCP и UDP с номером 53 (DNS) и порт UDP с номером 67 (DHCP). Если в качестве брадмауэра используется `firewalld`, то выполните следующую команду:

	sudo firewall-cmd --permanent --add-service=\{dns,dhcp\}

При возникновении проблем с доступом в первую очередь необходимо проверить настройки брандмауэра.

# Поиск всех серверов DNS и DHCP в своей сети

Проверьте свою локальную сеть с помощью `nmap`. В следующем примере выполняется поиск всех открытых TCP-портов в локальной сети и обнаруживается открытый TCP-порт 53, который используется службой DNS. На это указывает текст 53/tcp open domain:

	sudo nmap --open 192.168.1.0/24
	Starting Nmap 7.70 ( https://nmap.org ) at 2021-05-23 13:25 PDT
	[...]
	Nmap scan report for dns-server.sqr3l.nut (192.168.1.10)
	Host is up (0.12s latency).
	Not shown: 998 filtered ports
	Some closed ports may be reported as filtered due to --defeat-rst-ratelimit
	
	PORT STATE SERVICE
	22/tcp open ssh
	53/tcp open domain
	[...]
	
	Nmap done: 256 IP addresses (3 hosts up) scanned in 81.38 seconds

По умолчанию `nmap` проверяет только TCP-порты. Но DNS-серверы прослушивают TCP- и UDP-порт 53, а DHCP — UDP-порт 67. Следующий пример ищет только открытые порты UDP 53 и 67:

	sudo nmap -sU -p 53,67 192.168.1.0/24
	Starting Nmap 7.80 ( https://nmap.org ) at 2021-05-27 18:05 PDT
	
	Nmap scan report for dns-server.sqr3l.nut (192.168.1.10)
	Host is up (0.085s latency).
	
	PORT STATE SERVICE
	53/udp open domain
	67/udp open|filtered dhcps
	
	Nmap done: 256 IP addresses (3 hosts up) scanned in 13.85 seconds

Сканер `nmap` нашел только один сервер DNS/DHCP — на dns-server.sqr3l.nut.

Следующая команда ищет все открытые порты TCP и UDP в сети:

	sudo nmap -sU -sT 192.168.1.0/24

Поиск займет несколько минут, после чего у вас будет список активных служб на всех узлах сети, включая все службы, работающие на нестандартных портах.

Наличие нескольких серверов имен может приводить к конфликтам, и в любом случае важно знать, работают ли ваши пользователи на каких-либо серверах.
