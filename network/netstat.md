`netstat` печатает информацию о сетевой подсистеме Linux. Тип выводимой информации определяется первым аргументом, как описано ниже:

- `(none)` - По умолчанию netstat выводит список открытых сокетов. Если вы не укажете ни одного семейства адресов, то будут выведены активные сокеты всех сконфигурированных семейств адресов.
- `−−route, −r` - Отображение таблиц маршрутизации ядра.
- `−−groups, −g` - Отображение информации о членстве в многоадресных группах для IPv4 и IPv6.
- `−−interfaces=iface , −I=iface , −i` - Отображение таблицы всех сетевых интерфейсов или указанного iface.
- `−−masquerade, −M` - Отображение списка маскарадных соединений.
- `−−statistics, −s` - Отображение сводной статистики для каждого протокола.

# СИНТАКСИС

	netstat [address_family_options] [−−tcp|−t] [−−udp|−u] [−−udplite|−U] [−−sctp|−S] [−−raw|−w] [−−l2cap|−2] [−−rfcomm|−f] [−−listening|−l] [−−all|−a] [−−numeric|−n] [−−numeric−hosts] [−−numeric−ports] [−−numeric−users] [−−symbolic|−N] [−−extend|−e[−−extend|−e]] [−−timers|−o] [−−program|−p] [−−verbose|−v] [−−continuous|−c] [−−wide|−W] [delay]

	netstat {−−route|−r} [address_family_options] [−−extend|−e[−−extend|−e]] [−−verbose|−v] [−−numeric|−n] [−−numeric−hosts] [−−numeric−ports] [−−numeric−users] [−−continuous|−c] [delay]

	netstat {−−interfaces|−I|−i} [−−all|−a] [−−extend|−e] [−−verbose|−v] [−−program|−p] [−−numeric|−n] [−−numeric-hosts] [−−numeric-ports] [−−numeric-users] [−−continuous|−c] [delay]

	netstat {−−groups|−g} [−−numeric|−n] [−−numeric−hosts] [−−numeric−ports] [−−numeric−users] [−−continuous|−c] [delay]

	netstat {−−masquerade|−M} [−−extend|−e] [−−numeric|−n] [−−numeric−hosts] [−−numeric−ports] [−−numeric−users] [−−continuous|−c] [delay]

	netstat {−−statistics|-s} [−−tcp|−t] [−−udp|−u] [−−udplite|−U] [−−sctp|−S] [−−raw|−w] [delay]

	netstat {−−version|−V}

	netstat {−−help|−h}

	address_family_options:

	[-4|−−inet] [-6|−−inet6] [−−protocol={inet,inet6,unix,ipx,ax25,netrom,ddp,bluetooth, ... } ] [−−unix|−x] [−−inet|−−ip|−−tcpip] [−−ax25] [−−x25] [−−rose] [−−ash] [−−bluetooth] [−−ipx] [−−netrom] [−−ddp|−−appletalk] [−−econet|−−ec]

# ПАРАМЕТРЫ

- `−−verbose, −v` - Подробно рассказать пользователю о том, что происходит. В частности, печатает полезную информацию о ненастроенных семействах адресов.
- `−c, −−continuous` - Это значит, что `netstat` будет печатать выбранную информацию каждую секунду непрерывно.
- `−e, −−extend` - Отображение дополнительной информации. Используйте эту опцию дважды для получения максимальной детализации.
- `−p, −−program` - Показать PID и имя программы, к которой принадлежит каждый сокет.
- `−l, −−listening` - Показать только прослушивающие сокеты. (По умолчанию они опущены).
- `−a, −−all` - Показать как прослушивающие, так и не прослушивающие (для TCP это означает установленные соединения) сокеты. С помощью опции `--interfaces`-  показываются неработающие интерфейсы
- `delay` - `netstat` будет циклически печатать статистику каждые `n` секунд.
