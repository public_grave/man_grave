`dig` - это гибкий инструмент для опроса серверов имен DNS. Он выполняет поиск DNS и отображает ответы, полученные от сервера (серверов) имен, к которым был сделан запрос.

# СИНТАКСИС

	dig  [@server]  [-b  address]  [-c  class]  [-f  filename]  [-k  filename]  [-m] [-p port#] [-q name] [-t type] [-v] [-x addr] [-y
    [hmac:]name:key] [ [-4] | [-6] ] [name] [type] [class] [queryopt...]
	
	dig [-h]
	
	dig [global-queryopt...] [query...]

# ПРОСТОЕ ИСПОЛЬЗОВАНИЕ

	dig @server имя тип

где:

server - имя или IP-адрес сервера имен, который необходимо запросить. Это может быть IPv4-адрес или IPv6-адрес. Если аргумент server является именем хоста, `dig` разрешает это имя перед запросом к этому серверу имен.

name - имя записи ресурса, которую необходимо найти.

type указывает, какой тип запроса требуется - ANY, A, MX, SIG и т.д. type может быть любым допустимым типом запроса. Если аргумент type не указан, `dig` выполняет поиск записи A.

# ПАРАМЕТРЫ

- `-4` - Этот параметр указывает, что должен использоваться только IPv4.
- `-6` - Этот параметр указывает, что должен использоваться только IPv6.
- `-b адрес[#порт]` - Этот параметр задает IP-адрес источника запроса. Адрес должен быть действительным адресом одного из сетевых интерфейсов хоста, или "0.0.0.0", или "::". Дополнительный порт может быть указан путем добавления #port.
- `-c class` - Этот параметр задает класс запроса. По умолчанию используется класс IN; другие классы - HS для записей Hesiod или CH для записей Chaosnet.
- `-f файл` - Этот параметр устанавливает пакетный режим, в котором `dig` считывает из указанного файла список запросов на поиск для обработки. Каждая строка в файле должна быть организована таким же образом, как если бы она была представлена в виде запроса к `dig` с помощью интерфейса командной строки.
- `-p порт` - Этот параметр отправляет запрос на нестандартный порт сервера вместо порта 53, используемого по умолчанию. Этот параметр используется для проверки сервера имен, который был настроен на прослушивание запросов на нестандартном порту.
- `-q имя` - Этот параметр указывает доменное имя для запроса. Это полезно для того, чтобы отличить имя от других аргументов.
- `-r` - Этот параметр указывает, что опции из ${HOME}/.digrc не должны считываться. Это полезно для скриптов, которым нужно предсказуемое поведение.
- `-t тип` - Этот параметр указывает тип записи ресурса для запроса, который может быть любым допустимым типом запроса. Если это тип записи ресурса, поддерживаемый в BIND 9, он может быть задан мнемоникой типа (например, NS или AAAA). По умолчанию используется тип запроса A, если не указана опция `-x` для указания обратного поиска.
- `-x addr` - Этот параметр задает упрощенный обратный поиск для сопоставления адресов с именами. Addr - это IPv4-адрес в точечно-десятичной системе счисления или IPv6-адрес, ограниченный двоеточием. При использовании опции `-x` нет необходимости указывать аргументы name, class и type. `dig` автоматически выполняет поиск по имени, например 94.2.0.192.in-addr.arpa, и устанавливает тип и класс запроса на PTR и IN соответственно. IPv6-адреса ищутся с использованием формата nibble в домене IP6.ARPA.

# ПАРАМЕТРЫ ЗАПРОСА

Каждый параметр запроса обозначается ключевым словом, которому предшествует знак плюс (+). Некоторые ключевые слова устанавливают или сбрасывают параметр; перед ними может стоять строка no, чтобы отменить значение этого ключевого слова. Другие ключевые слова присваивают параметрам значения, например интервал тайм-аута. Они имеют форму +ключевое слово=значение. Ключевые слова могут быть сокращены, если сокращение однозначно; например, +cd эквивалентно +cdflag.

+aaflag, +noaaflag
+aaonly, +noaaonly
+additional, +noadditional
+adflag, +noadflag
+all, +noall
+answer, +noanswer
+authority, +noauthority
+badcookie, +nobadcookie
+besteffort, +nobesteffort
+bufsize[=B]
+cd, +cdflag, +nocdflag
+class, +noclass
+cmd, +nocmd
+comments, +nocomments
+cookie=####, +nocookie
+crypto, +nocrypto
+dns64prefix, +nodns64prefix
+dnssec, +do, +nodnssec, +nodo
+domain=somename
+edns[=#], +noedns
+ednsflags[=#], +noednsflags
+ednsnegotiation, +noednsnegotiation
+ednsopt[=code[:value]], +noednsopt
+expire, +noexpire
+fail, +nofail
+fuzztime[=value], +nofuzztime
+header-only, +noheader-only
+https[=value], +nohttps
+https-get[=value], +nohttps-get
+http-plain[=value], +nohttp-plain
+http-plain-get[=value], +nohttp-plain-get
+identify, +noidentify
+idnin, +noidnin
+idnout, +noidnout
+ignore, +noignore
+keepalive, +nokeepalive
+keepopen, +nokeepopen
+multiline, +nomultiline
+ndots=D
+nsid, +nonsid
+nssearch, +nonssearch
+onesoa, +noonesoa
+opcode=value, +noopcode
+padding=value
+qid=value
+qr, +noqr
+question, +noquestion
+raflag, +noraflag
+recurse, +norecurse
+retry=T
+rrcomments, +norrcomments
+search, +nosearch
+short, +noshort
+showbadcookie, +noshowbadcookie
+showsearch, +noshowsearch
+split=W
+stats, +nostats
+subnet=addr[/prefix-length], +nosubnet
+tcflag, +notcflag
+tcp, +notcp
+timeout=T
+tls, +notls
+tls-ca[=file-name], +notls-ca
+tls-certfile=file-name, +tls-keyfile=file-name, +notls-certfile, +notls-keyfile
+tls-hostname=hostname, +notls-hostname
+trace, +notrace
+tries=T
+ttlid, +nottlid
+ttlunits, +nottlunits
+unknownformat, +nounknownformat
+vc, +novc
+yaml, +noyaml
+zflag, +nozflag
