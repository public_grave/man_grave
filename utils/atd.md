`atd` запускает задания, поставленные в очередь `at`.

# СИНТАКСИС

	atd [−l load_avg] [−b batch_interval] [−d] [−f] [−n] [−s]
