`gapplication` может использоваться для запуска приложений, у которых DBusActivatable в файлах .desktop имеет значение true, и может использоваться для отправки сообщений уже запущенным экземплярам других приложений. Приложения могут ссылаться на `gapplication` в строке Exec своего файла .desktop, чтобы поддерживать обратную совместимость с реализациями, которые не поддерживают DBusActivatable напрямую.

# СИНТАКСИС

	gapplication help [COMMAND]
    gapplication version
    gapplication list-apps
    gapplication launch APPID
    gapplication launch APPID [FILE...]
    gapplication list-actions APPID
    gapplication action APPID ACTION [PARAMETER]
