`imsettings-info` отображает информацию о методе ввода. если не указано имя метода ввода или имя конфигурации, то отображается тот метод, который активен в данный момент.

# СИНТАКСИС

	imsettings-info [ -h ] [ --help ] [имя метода ввода | имя файла конфигурации]
