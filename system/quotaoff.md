`quotaoff` - выключение квот на файловой системе.

# СИНТАКСИС

	quotaoff [ -vugPp ] [ -x state ] файловая_система...
	quotaoff [ -avugp ]
