Команда `getsubids` выводит список диапазонов идентификаторов подчиненных пользователей для данного пользователя. Идентификаторы подчиненных групп могут быть перечислены с помощью параметра `-g`. В выводе команды указывается (в порядке слева направо) индекс списка, имя пользователя, начало диапазона UID и количество UID в диапазоне.

# СИНТАКСИС

	getsubids [-gh] ПОЛЬЗОВАТЕЛЬ

# ВЕРСИЯ

	shadow-utils 4.14.0
	10/03/2023
