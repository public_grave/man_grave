Команда `chrt` используется для управления приоритетами планировщика задач. Она устанавливает или извлекает атрибуты планирования в реальном времени существующего PID или запускает команду с заданными атрибутами.

# СИНТАКСИС

	chrt [ПАРАМЕТРЫ] ПРИОРИТЕТ КОМАНДА АРГУМЕНТ
	chrt [ПАРАМЕТРЫ] -p [ПРИОРИТЕТ] PID

Где ПРИОРИТЕТ - это приоритет исполнения процесса, который может быть задан числом от 1 до 99. Число меньше означает более высокий приоритет. По умолчанию приоритет равен 0, что обозначает обычный режим работы без приоритетов реального времени.

Для использования этой команды обычно требуются привилегии root.

# ПОЛИТИКИ

- `−o, −−other` - Установка политики планирования на `SCHED_OTHER` (планирование с разделением времени). Это политика планирования Linux по умолчанию.
- `−f, −−fifo` - Установка политики планирования на `SCHED_FIFO` (первый вошел-первый вышел). Это процесс реального времени, поддерживающий только одну очередь, которая представляет собой порядок процессов.
- `−r, −−rr` - Установка политики планирования на `SCHED_RR` (Round Robin - планирование по кругу). Если политика не определена, `SCHED_RR` используется по умолчанию.
- `−b, −−batch` - Установка политики планирования в `SCHED_BATCH` (планирование пакетных процессов). Аргумент приоритета должен быть равен нулю.
- `−i, −−idle` - Установка политики планирования в `SCHED_IDLE` (планирование заданий с очень низким приоритетом). Аргумент приоритета должен быть равен нулю.
- `−d, −−deadline` - Установка политики планирования в `SCHED_DEADLINE` (планирование крайних сроков в модели спорадических задач).

# ПАРАМЕТРЫ

- `−a, −−all−tasks` - Установка или получение атрибутов планирования всех задач (потоков) для заданного PID.
- `−m, −−max` - Показать минимальный и максимальный допустимые приоритеты, затем выйти.
- `−p, −−pid` - Работает с существующим PID и не запускает новую задачу.
- `−v, −−verbose` - Показать информацию о состоянии.
- `−h, −−help` - Вывести текст справки и выйти.
- `−V, −−version` - Вывести информацию о версии и выйти.

# ПРИМЕРЫ

получить атрибуты реального времени существующей задачи

	chrt -p PID

запустить процесс с приоритетом 10

	chrt -p 10 command

изменить политику планирования на `SCHED_FIFO` на уровне 20 для процесса с идентификатором PID 1234

	chrt -f -p 20 1234

сбросить приоритеты на значения по умолчанию для процесса

	chrt -o -p 0 PID

# ВЕРСИЯ

	util-linux 2.39.4
	2024-01-31
