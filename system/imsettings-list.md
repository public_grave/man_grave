`imsettings-list` отображает список методов ввода, которые в данный момент управляются imsettings. В начале имени могут появляться следующие символы, указывающие на статус:

- `*` Метод ввода запущен.
- `-` Метод ввода должен быть активен, но не запущен.

# СИНТАКСИС

	imsettings-list
