`imsettings-switch` переключает метод ввода, который управляется imsettings. Требуется либо имя метода ввода, либо имя файла конфигурации, если не указана опция `--read-xinputrc` для определения цели из `$HOME/.config/imsettings/xinputrc` или `$xinputrcdir/xinputrc`.

# СИНТАКСИС

	imsettings-switch [ -hnqrx ] [Имя метода ввода | имя файла конфигурации]

# ПАРАМЕТРЫ

- `-n --no-update` - Не обновлять каталог `$HOME/imsettings/xinputrc`. Без этой опции `imsettings-switch` обновит его с указанием метода ввода.
- `-q --quiet` - Не показывать никаких сообщений, кроме ошибок.
- `-r --restart` - Перезапустить заданный метод ввода или тот, который определяется из xinputrc с помощью опции `--read-xinputrc`.
- `-x --read-xinputrc` - Определить целевой метод ввода из `$HOME/imsettings/xinputrc` или `$xinputrcdir/xinputrc`, если он недоступен.
- `-h --help` - Показать краткое описание опций.

- `Имя метода ввода` - Переключение метода ввода на имя метода ввода.
- `имя файла конфигурации` - Переключение метода ввода на имя файла конфигурации.
