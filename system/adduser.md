`useradd` − регистрирует нового пользователя или изменяет информацию по умолчанию о новых пользователях.

# СИНТАКСИС

	useradd [options] LOGIN
	useradd −D
	useradd −D [options]
