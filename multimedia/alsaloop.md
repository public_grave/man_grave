`alsaloop` позволяет создать PCM-петлю между устройством захвата PCM и устройством воспроизведения PCM. `alsaloop` поддерживает несколько звуковых карт, адаптивную синхронизацию часов, адаптивную передискретизацию частоты с помощью библиотеки samplerate (если она доступна в системе). Кроме того, элементы управления микшером могут быть перенаправлены с одной карты на другую (например, Master и PCM).

# СИНТАКСИС

	alsaloop [-option] [cmd]
	
# ПАРАМЕТРЫ

- `-g <file> | --config=<file>` - Использование заданного файла конфигурации.
- `-P <device> | --pdevice=<device>` - Использовать заданное устройство воспроизведения.
- `-C <device> | --cdevice=<device>` - Использовать заданное устройство захвата.
- `-r <rate> | --rate=<rate>` - Спецификация частоты. Значение по умолчанию - 48000 (Гц).
- `-t <usec> | --tlatency=<usec>` - Запрашиваемая задержка в мсек (1/1000000 сек).

# ПРИМЕРЫ

	alsaloop -C hw:0,0 -P hw:1,0 -t 50000
