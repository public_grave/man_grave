Программа `magick-script` входит в набор инструментов ImageMagick.  С ее помощью можно конвертировать изображения в разные форматы, а также изменять их размер, блюрить, обрезать, обесцвечивать, сглаживать, накладывать, переворачивать, соединять, передискретизировать и многое другое. 

# СИНТАКСИС

	magick-script [input-options] input-file [output-options] output-file
	
# ПРИМЕР СКРИПТА

	#!/bin/env magick-script
	-size 100x100 xc:red ( rose: -rotate -90 ) +append  -write show:
