`mke2fs` используется для создания файловой системы ext2, ext3 или ext4, обычно в разделе диска (или файле), названном по устройству. Размер файловой системы задается fs-size. Если параметр fs-size опущен, mke2fs создаст файловую систему на основе размера устройства. Если mke2fs запускается как mkfs.XXX (то есть mkfs.ext2, mkfs.ext3 или mkfs.ext4), то подразумевается опция `-t XXX`. Параметры по умолчанию для вновь созданной файловой системы, если они не переопределены опциями, перечисленными ниже, управляются конфигурационным файлом `/etc/mke2fs.conf`. Дополнительные сведения см. на странице руководства `mke2fs.conf`.

# СИНТАКСИС

	mke2fs  [  -c | -l filename ] [ -b block-size ] [ -C cluster-size ] [ -d root-directory ] [ -D ] [ -g blocks-per-group ] [ -G num‐
    ber-of-groups ] [ -i bytes-per-inode ] [ -I inode-size ] [ -j ] [ -J journal-options ] [ -N number-of-inodes ] [ -n  ]  [  -m  re‐
    served-blocks-percentage ] [ -o creator-os ] [ -O [^]feature[,...]  ] [ -q ] [ -r fs-revision-level ] [ -E extended-options ] [ -v
    ] [ -F ] [ -L volume-label ] [ -M last-mounted-directory ] [ -S ] [ -t fs-type ] [ -T usage-type ] [ -U UUID ] [ -V ] [ -e errors-
    behavior ] [ -z undo_file ] device [ fs-size ]

    mke2fs -O journal_dev [ -b block-size ] [ -L volume-label ] [ -n ] [ -q ] [ -v ] external-journal [ fs-size ]
