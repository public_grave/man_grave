`edquota` - это редактор квот. В командной строке можно указать одного или несколько пользователей, групп или проектов. Если вместо имени пользователя/группы/проекта указано число, оно рассматривается как UID/GID/Project ID. Для каждого пользователя, группы или проекта создается временный файл с ASCII-представлением текущих дисковых квот для этого пользователя, группы или проекта, а затем вызывается редактор этого файла. Затем квоты могут быть изменены, добавлены новые и т. д. Установка квоты в ноль означает, что квота не должна накладываться.

# СИНТАКСИС

	edquota [ -p protoname ] [ -u | -g | -P ] [ -rm ] [ -F format-name ] [ -f filesystem ] username | groupname | projectname...
	edquota [ -u | -g | -P ] [ -F format-name ] [ -f filesystem ] -t
	edquota [ -u | -g | -P ] [ -F format-name ] [ -f filesystem ] -T username | groupname | projectname...
