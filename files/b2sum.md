Команда `b2sum` используется для вычисления криптографических контрольных сумм BLAKE2.

# СИНТАКСИС

	b2sum [ПАРАМЕТР]... [ФАЙЛ]...

# ПАРАМЕТРЫ

- `-c, --check` - считывание контрольных сумм из ФАЙЛОВ и их проверка

# ВЕРСИЯ

	GNU coreutils 9.3
	January 2024
