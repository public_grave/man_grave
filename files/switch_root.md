`switch_root` перемещает уже смонтированные /proc, /dev, /sys и /run в newroot, делает newroot новой корневой файловой системой и запускает процесс init. `switch_root` рекурсивно удаляет все файлы и каталоги на текущей корневой файловой системе.

# СИНТАКСИС

	switch_root newroot init [arg...]

# ВЕРСИЯ

	util-linux 2.39.4
	2024-01-31
