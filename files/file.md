`file` - определить тип файла.

# СИНТАКСИС

	file [-bcdEhiklLNnprsSvzZ0] [--apple] [--exclude-quiet] [--extension] [--mime-encoding] [--mime-type] [-e testname] [-F separator]
         [-f namefile] [-m magicfiles] [-P name=value] file ...
	file -C [-m magicfiles]
	file [--help]
	
# ПАРАМЕТРЫ

- `-b, --brief` - Не добавлять имена файлов к строкам вывода (краткий режим).
- `-i, --mime` - Заставляет команду `file` выводить строки типа mime, а не более традиционные человекочитаемые.  Таким образом, вместо "ASCII-text" может быть написано "text/plain; charset=us-ascii".
- `-p, --preserve-date` - В системах, поддерживающих utime или utimes, попытка сохранить время доступа к анализируемым файлам, чтобы сделать вид, что `file` никогда их не читал.
- `-z, --uncompress` - Попытка заглянуть внутрь сжатых файлов.
