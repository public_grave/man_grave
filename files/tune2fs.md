`tune2fs` позволяет настраивать различные параметры файловой системы в ext2, ext3 или ext4. Текущие значения этих параметров могут быть отображены с помощью опции `-l` программы `tune2fs` или с помощью программы `dumpe2fs`.

# СИНТАКСИС

	tune2fs [ -l ] [ -c max-mount-counts ] [ -e errors-behavior ] [ -f ] [ -i interval-between-checks ] [ -I new_inode_size ] [ -j ] [
    -J  journal-options  ] [ -m reserved-blocks-percentage ] [ -o [^]mount-options[,...]  ] [ -r reserved-blocks-count ] [ -u user ] [
    -g group ] [ -C mount-count ] [ -E extended-options ] [ -L volume-label ] [ -M last-mounted-directory ] [ -O [^]feature[,...]  ] [
    -Q quota-options ] [ -T time-last-checked ] [ -U UUID ] [ -z undo_file ] device
