`dircolors` - настройка цвета для `ls`.

# СИНТАКСИС

	dircolors [ПАРАМЕТР]... [ФАЙЛ]

- `-b, --sh, --bourne-shell` - вывести код Bourne shell для установки LS_COLORS
- `-c, --csh, --c-shell` - вывести код C shell для установки LS_COLORS
- `-p, --print-database` - вывести значения по умолчанию
- `--print-ls-colors` - вывести полностью экранированные цвета для отображения на экране
