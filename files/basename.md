Команда `basename` удаляет имена каталогов или суффикс и отображает последнюю часть имени файла.

# СИНТАКСИС

	basename ИМЯ [СУФФИКС]
	basename ПАРАМЕТР... ИМЯ...

# ПАРАМЕТРЫ

- `-a, --multiple` — поддерживает несколько аргументов и обрабатывает каждый из них как ИМЯ
- `-s, --suffix=SUFFIX` — удаляет завершающий SUFFIX, такой как расширение файла
- `-z, --zero` - завершает каждую строку вывода символом NUL, а не новой строкой
- `--help` - вывести справку и выйти	
- `--version` - вывести информацию о версии и выйти

# ПРИМЕРЫ

	basename /usr/bin/sort # Вывод: sort
	basename -a file.txt /home/user/other.txt # Вывод: file.txt other.txt
	basename -s .txt file.txt # Вывод: file
	basename /home/user/file.txt .txt # Вывод: file

# ВЕРСИЯ

	GNU coreutils 9.3
	January 2024
