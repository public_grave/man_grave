Команда `xdg-open` открывает файл или URL-адрес через предпочитаемое пользователем или используемое по умолчанию приложение или браузер.

# ПРИМЕРЫ

	xdg-open https://www.freedesktop.org/
	xdg-open /tmp/foobar.png
