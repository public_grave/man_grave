`ex` - Запуск `vim` в режиме Ex (execute). Можно также сделать это с помощью аргумента "-e". Перейти в обычный режим можно с помощью команды ":vi".
