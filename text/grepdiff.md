Для каждого файла, измененного патчем, если патч содержит REGEX, выводится имя файла.

# СИНТАКСИС

	grepdiff [[-n] | [--line-number]] [[-N] | [--number-files]] [[-p n] | [--strip-match=n]] [--strip=n] [--addprefix=PREFIX]
             [--addoldprefix=PREFIX] [--addnewprefix=PREFIX] [[-s] | [--status]] [[-i PATTERN] | [--include=PATTERN]] [[-I FILE] |
             [--include-from-file=FILE]] [[-x PATTERN] | [--exclude=PATTERN]] [[-X FILE] | [--exclude-from-file=FILE]] [[-# RANGE] |
             [--hunks=RANGE]] [--lines=RANGE] [[-FRANGE] | [--files=RANGE]] [--annotate] [--as-numbered-lines=WHEN] [--format=FORMAT]
             [--remove-timestamps] [[-v] | [--verbose]] [[-z] | [--decompress]] [[-E] | [--extended-regexp]] [[-H] |
             [--with-filename]] [[-h] | [--no-filename]] [--output-matching=WHAT] [--only-match=WHAT] {[REGEX] | [-f FILE]} [file...]

    grepdiff {[--help] | [--version] | [--list] | [--filter ...]}
