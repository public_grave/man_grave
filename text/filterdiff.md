`filterdiff` - извлечь или исключить отличия из diff-файла.

# СИНТАКСИС

	filterdiff [[-i PATTERN] | [--include=PATTERN]] [[-I FILE] | [--include-from-file=FILE]] [[-p n] | [--strip-match=n]] [--strip=n]
               [--addprefix=PREFIX] [--addoldprefix=PREFIX] [--addnewprefix=PREFIX] [[-x PATTERN] | [--exclude=PATTERN]] [[-X FILE] |
               [--exclude-from-file=FILE]] [[-v] | [--verbose]] [--clean] [[-z] | [--decompress]] [[-# RANGE] | [--hunks=RANGE]]
               [--lines=RANGE] [[-FRANGE] | [--files=RANGE]] [--annotate] [--format=FORMAT] [--as-numbered-lines=WHEN]
               [--remove-timestamps] [file...]
	filterdiff {[--help] | [--version] | [--list] | [--grep ...]}
