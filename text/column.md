Утилита `column` форматирует входные данные в несколько колонок. Утилита поддерживает три режима:

- Столбцы заполняются раньше строк. Это режим по умолчанию (требуется для обратной совместимости).
- Строки заполняются раньше столбцов. Этот режим включается параметром `-x, --fillrows`.
- Таблица. Определить количество столбцов, содержащихся во входных данных, и создать таблицу. Этот режим включается параметром `-t, --table`, а форматирование столбцов можно изменить параметром `--table-*`. Используйте этот режим, если не уверены. Вывод выравнивается по ширине терминала в интерактивном режиме и по 80 колонкам в неинтерактивном режиме (подробнее см. `--output-width`).

Ввод осуществляется из файла или, в противном случае, из стандартного ввода. Пустые строки игнорируются, а все недопустимые многобайтовые последовательности кодируются по соглашению x<hex>.

# СИНТАКСИС

	column [параметры] [файл ...]

# ВЕРСИЯ

	util-linux 2.39.4
	2024-01-31
