Отправляет текстовый файл(ы), stdin или буфер обмена в пастбин сообщества Fedora по адресу https://paste.centos.org и возвращает URL.

# СИНТАКСИС

	fpaste [ПАРАМЕТР]... [ФАЙЛ]...
