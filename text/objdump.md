Команда `objdump` используется для анализа объектных файлов, исполняемых файлов, библиотек и т. д. Она может выдавать информацию о исполняемом машинном коде, именах функций и их местоположения, отладочных символах, заголовках файлов и разделов и многое другое.

# ПАРАМЕТРЫ

- `-d, --disassemble, --disassemble=symbol` - Отображение ассемблерных мнемоник для машинных инструкций из входного файла. Эта опция дизассемблирует только те секции, которые, как ожидается, содержат инструкции
- `-D, --disassemble-all` - Аналогично -d, но разбирает содержимое всех секций, а не только тех, которые должны содержать инструкции
- `-f, --file-headers` -  Отображение сводной информации из общего заголовка каждого из файлов объектного файла
- `-h, --section-headers, --headers` - Отображение сводной информации из заголовков секций объектного файла
- `-t, --syms` - Печать записей таблицы символов в файле

# ВЕРСИЯ

	binutils-2.40.00
	2024-01-25
