Команда `numfmt` используется для преобразования чисел из и в человекопонятные формы представления. Она может работать с различными видами числовых данных и поддерживает конвертацию между единицами измерений, такими как байты, килобайты и так далее. Например, команда `numfmt` можно использовать для перевода числа из системы единиц (SI) в единицы информационного обмена (IEC) или обратно. Команда также предлагает множество опций для кастомизации выходных данных.

# СИНТАКСИС

	numfmt [ПАРАМЕТРЫ]... [ЧИСЛО]...

# ПРИМЕРЫ

	numfmt --to=iec 1024 => 1K
	numfmt --to=si 2000 => 2.0K
	numfmt --to=iec-i 4096 => 4.0Ki
	echo 1K | numfmt --from=si => 1000
	echo 1K | numfmt --from=iec => 1024
	df | numfmt --header --field 2-4 --to=iec

# ВЕРСИЯ

	GNU coreutils 9.3
	January 2024
