`podman-healthcheck-run` - запустить проверку состояния контейнера.

Коды ошибок:

- 0 = команда healthcheck успешно выполнена
- 1 = команда healthcheck не выполнена
- 125 = произошла ошибка

# СИНТАКСИС

	podman healthcheck run container
