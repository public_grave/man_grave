`grub-mount` - экспорт файловой системы GRUB с помощью FUSE. Инструмент отладки для драйвера файловой системы.

# СИНТАКСИС

	grub-mount [OPTION...] IMAGE1 [IMAGE2 ...] MOUNTPOINT
