`crond` - демон для выполнения команд по расписанию.

# СИНТАКСИС

	crond [-c | -h | -i | -n | -p | -P | -s | -m<mailcommand>]
	crond -x [ext,sch,proc,pars,load,misc,test,bit]
	crond -V
