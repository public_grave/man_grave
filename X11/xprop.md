`xprop`  — это утилита для отображения свойств окон и шрифтов в X-сервере. Она позволяет выбрать окно или шрифт с помощью аргументов командной строки или кликнув на желаемое окно. Затем она выводит список свойств.

# ПАРАМЕТРЫ

- `-help` — Выводит краткое описание доступных аргументов
- `-grammar` — Выводит подробное описание синтаксиса командной строки
- `-id id` — Выбирает окно с указанным идентификатором `id` для исследования
- `-root` — Выбирает корневое окно `root` для исследования
- `-name name` — Выбирает окно с указанным именем `name` для отображения свойств
- `-frame` — Не игнорировать фреймы оконного менеджера
- `-font name` — Выбирает шрифт `name` для отображения свойств
- `-display host:dpy` — X-сервер, к которому нужно подключиться
