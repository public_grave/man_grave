`GNU cpio` - это инструмент для создания и извлечения архивов, а также копирования файлов из одного места в другое. Он работает с несколькими форматами `cpio`, а также читает и записывает tar-файлы.

Поддерживаются следующие форматы архивов: двоичный, старый ASCII, новый ASCII, crc, двоичный HPUX, старый ASCII HPUX, старый tar и POSIX.1 tar. Формат tar предоставляется для совместимости с программой `tar`. По умолчанию `cpio` создает архивы в двоичном формате, для совместимости со старыми программами `cpio`. При извлечении из архивов `cpio` автоматически распознает тип читаемого архива и может читать архивы, созданные на машинах с другим порядком байт.

# СИНТАКСИС

## Режим copy-out

В режиме copy-out `cpio` копирует файлы в архив. Он считывает список имен файлов, по одному на строку, на стандартный вход и записывает архив на стандартный выход. Обычно список имен файлов формируется с помощью команды `find`; чтобы свести к минимуму проблемы с правами доступа к нечитаемым каталогам, следует задать `find` опцию `-depth`.

	cpio  {-o|--create} [-0acvABLV] [-C bytes] [-H format] [-D DIR] [-M message] [-O [[user@]host:]archive] [-F [[user@]host:]archive]
    [--file=[[user@]host:]archive] [--format=format] [--warning=FLAG]  [--message=message][--null]  [--reset-access-time]  [--verbose]
    [--dot] [--append] [--block-size=blocks] [--dereference] [--io-size=bytes] [--rsh-command=command]  [--license] [--usage] [--help]
    [--version] < name-list [> archive]

## Режим copy-in

В режиме copy-in `cpio` копирует файлы из архива или выводит список содержимого архива. Он считывает архив со стандартного ввода. Любые аргументы командной строки, не являющиеся параметрами, представляют собой шаблоны оболочки; только те файлы в архиве, имена которых соответствуют одному или нескольким из этих шаблонов, копируются из архива. В отличие от оболочки, начальное "." в имени файла соответствует подстановочному символу в начале шаблона, а "/" в имени файла может соответствовать подстановочному знаку. Если шаблоны не заданы, извлекаются все файлы.

	cpio  {-i|--extract}  [-bcdfmnrtsuvBSV]  [-C  bytes]  [-E  file]  [-H  format]  [-D  DIR]  [-M message] [-R [user][:.][group]] [-I
    [[user@]host:]archive] [-F [[user@]host:]archive] [--file=[[user@]host:]archive] [--make-directories] [--nonmatching] [--preserve-
    modification-time] [--numeric-uid-gid] [--rename] [-t|--list] [--swap-bytes] [--swap] [--dot]  [--warning=FLAG]  [--unconditional]
    [--verbose]     [--block-size=blocks]     [--swap-halfwords]     [--io-size=bytes]     [--pattern-file=file]     [--format=format]
    [--owner=[user][:.][group]] [--no-preserve-owner] [--message=message] [--force-local] [--no-absolute-filenames]  [--absolute-file‐
    names]  [--sparse]  [--only-verify-crc] [--to-stdout] [--quiet] [--ignore-devno] [--renumber-inodes] [--device-independent] [--re‐
    producible] [--rsh-command=command] [--license] [--usage] [--help] [--version] [pattern...] [< archive]

## Режим copy-pass

В режиме copy-pass `cpio` копирует файлы из одного дерева директорий в другое, совмещая этапы copy-out и copy-in без использования архива. Он считывает список файлов для копирования со стандартного ввода; директория, в которую он будет их копировать, задается в качестве аргумента без параметра.

	cpio {-p|--pass-through} [-0adlmuvLV] [-R [user][:.][group]] [-D DIR] [--null] [--reset-access-time] [--make-directories] [--link]
    [--quiet]    [--preserve-modification-time]    [--unconditional]    [--verbose]    [--dot]    [--warning=FLAG]     [--dereference]
    [--owner=[user][:.][group]]  [--no-preserve-owner]  [--sparse]  [--license] [--usage] [--help] [--version] destination-directory <
    name-list

# Основные режимы операций

- `-i, --extract` - Извлечение файлов из архива (выполняется в режиме copy-in).
- `-o, --create` - Создать архив (выполняется в режиме copy-out).
- `-p, --pass-through` - Запуск в режиме copy-pass.
- `-t, --list` - Вывести список содержимого входного файла.
