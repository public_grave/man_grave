`dump-utmp` - вывод utmp-файла в удобочитаемом формате.

# СИНТАКСИС

	dump-utmp [-hrR] [-n <recs>] <files>
