Команда `slabtop` используется для отображения подробной информации о кэше ядра в реальном времени. Она показывает список наиболее значимых кэшей, отсортированных по одному из критериев сортировки. Также отображается заголовок статистики, заполненный информацией о слоях слэбов.

Команда `slabtop` отображает следующие категории памяти:

1. ACTIVE - Количество активных объектов (a).
2. OBJ/SLAB - Количество объектов на каждый слэб (b).
3. CACHE SIZE - Размер кэша (c).
4. SLABS - Количество слэбов (l).
5. N/A - Количество активных слэбов (v).
6. NAME - Название (n).
7. OBJS - Количество объектов (o).
8. N/A - Количество страниц на слэб (p).
9. OBJ SIZE - Размер объекта (s).
10. USE - Использование кэша (u).

По умолчанию критерием сортировки является сортировка по количеству объектов ("o"). Критерии сортировки также можно изменить во время работы `slabtop`, нажав соответствующий символ.

# СИНТАКСИС

	slabtop [ ПАРАМЕТРЫ ]

# ПАРАМЕТРЫ

- `−d, −−delay=N` - Обновление дисплея каждые n секунд. По умолчанию slabtop обновляет дисплей каждые три секунды. Чтобы выйти из программы, нажмите q. Этот параметр нельзя сочетать с параметром `-o`
- `−s, −−sort=S` - Сортировка по S, где S - один из критериев сортировки
- `−o, −−once` - Отобразить вывод один раз и затем выйти
- `−V, −−version` - Вывести информацию о версии и выйти
- `−h, −−help` - Вывести информацию об использовании и выйти

# ВЕРСИЯ

	procps-ng
	2021-03-11
