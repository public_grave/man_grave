`getopt` используется для разбора (парсинга) параметров в командных строках для облегчения разбора процедурами оболочки, а также для проверки наличия допустимых параметров.

# СИНТАКСИС

	getopt optstring parameters
	getopt [options] [--] optstring parameters
	getopt [options] -o|--options optstring [options] [--] parameters
