Выход с кодом состояния, указывающим на неудачу.

# СИНТАКСИС

	false [ignored command line arguments]
	false OPTION

# ВЕРСИЯ

	GNU coreutils 9.3
	January 2024
