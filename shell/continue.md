Возобновляет следующую итерацию вложенного цикла `for`, `while`, `until` или `select`. Если указано `n`, то возобновление происходит на n-ом вложенном цикле.  `n` должно быть ≥ 1. Если `n` больше количества вложенных циклов, то возобновляется последний вложенный цикл (цикл "верхнего уровня").

# СИНТАКСИС

	continue [n]

# ПРИМЕР: Исключение определенных значений из вывода

```sh
LIMIT=19
echo
echo "Печать чисел от 1 до 20 (исключая 3 и 11)."
a=0
while [ $a -le "$LIMIT" ]
do
	a=$(($a+1))
	if [ "$a" -eq 3 ] || [ "$a" -eq 11 ]; then
		continue
	fi
	echo -n "$a "
done
```
