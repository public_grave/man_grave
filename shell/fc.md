`fc` используется для вывода списка или редактирования и повторного выполнения команд из списка истории. FIRST и LAST могут быть числами, задающими диапазон, или FIRST может быть строкой, что означает последнюю команду, начинающуюся с этой строки.

# СИНТАКСИС

	fc [-e ename] [-lnr] [first] [last]
	fc -s [pat=rep] [cmd]

# ПАРАМЕТРЫ

- `-e ENAME` - выбирает, какой редактор использовать.  По умолчанию используется FCEDIT, затем EDITOR, затем vi
- `-l` - вывести список строк вместо редактирования
- `-n` - не указывать номера строк при выводе списка
- `-r` - обратный порядок строк (самые новые перечислены первыми)
