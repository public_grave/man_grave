`dnf-debuginfo-install` - установка связанных пакетов отладочной информации для заданных пакетов.

# СИНТАКСИС

	dnf debuginfo-install <pkg>
	
# КОНФИГУРАЦИЯ

	/etc/dnf/plugins/debuginfo-install.conf
