`rpm2archive` преобразует файлы .rpm, указанные в качестве аргументов, в tar-архивы на стандартном выходе.

# СИНТАКСИС

	rpm2archive {-n|--nocompression} {-f|--format=pax|cpio} ФАЙЛЫ
